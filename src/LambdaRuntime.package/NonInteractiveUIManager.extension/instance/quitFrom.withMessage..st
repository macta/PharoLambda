*LambdaRuntime
quitFrom: aContext withMessage: aString
	"log error and quit.
	Overriden to provide a more minimal error and then fuel out the context"

	[ Smalltalk logStdErrorDuring: [ :stderr|
				stderr
					cr;
					nextPutAll: '--- RUNNING ERROR HANDLER ---'; cr;
					nextPutAll: aString; cr; cr ].
			
		self writeFuelContext: aContext.
		
		Smalltalk logError: aString inContext: aContext. ]
			ensure: [ self exitFailure ]